//Primitives : number, string, boolean
//More complex types : arrays, objects
//Function types, parameters

//Primitives

// let age: number = 24;
let age: number;
age = 12;

let userName: string;
userName = "jahnvi";

let isLoading: boolean;
isLoading = true;

//More complex

let hobbies: string[];
hobbies = ["xyz", "abc"];

let person: {
  name: string;
  age: number;
};

person = {
  name: "Jahnvi",
  age: 21,
};

// person = {
//   isValid: true, this will give us an error
// };

//combining both

let people: {
  name: string;
  age: number;
}[];

//type inference

let course = "React + TypeScript";

// course = 122; //It will give us error

//union types

let course1: string | number = "react + typeScript";
course1 = 12;

// type alias

type Person = {
  name: string;
  age: number;
};

let p1: Person;
let p2: Person[];

//functions and types
function sub(a: number, b: number) {
  return a + b;
}

function printOutput(value: any) {
  console.log(value);
}

//Generics

function insertArray<T>(array: T[], value: T) {
  const newArray = [value, ...array];
  return newArray;
}

const demoArray = [1, 2, 3];

const updatedArray = insertArray(demoArray, -1); //[-1,1,2,3]
const stringArray = insertArray(["a", "b", "c"], "d");

// updatedArray[0].split("");
// let numbers: Array<number> = [1, 2, 3];
