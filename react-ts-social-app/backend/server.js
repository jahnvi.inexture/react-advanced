const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const { MONGOURI } = require("./keys");

require("./models/user");
require("./models/postModel");

const app = express();
app.use(express.json());
app.use(cors());

mongoose.connect(MONGOURI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
mongoose.connection.on("connected", () => {
  console.log("database connected");
});
mongoose.connection.on("error", (err) => {
  console.log("database connection failed", err);
});

app.use(require("./routes/authRoutes"));
app.use(require("./routes/postRoutes"));

// Use body-parser to retrieve the raw body as a buffer
const bodyParser = require("body-parser");

// Add middleware for parsing JSON and urlencoded data and populating `req.body`
app.use(express.json({ limit: "50mb" }));
app.use(express.urlencoded({ limit: "50mb", extended: true }));

// parse various different custom JSON types as JSON
app.use(bodyParser.json());

const PORT = 8000;
app.listen(PORT, () => {
  console.log(`Server is listening on port ${PORT}`);
});
