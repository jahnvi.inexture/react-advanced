const Post = require("../models/postModel");

const getAllPost = async (req, res) => {
  Post.find()
    .populate("postedBy", "_id name")
    .populate("comments.postedBy", "_id name")
    .then((posts) => {
      res.json({ posts });
    })
    .catch((err) => {
      res.status(500).json({ error: err.message });
    });
};

const getAllMyPost = async (req, res) => {
  Post.find({ postedBy: req.user._id })
    .populate("postedBy", "_id name image")
    .then((myposts) => {
      res.json({ myposts });
    })
    .catch((err) => {
      res.status(500).json({ error: err.message });
    });
};

const createPost = async (req, res) => {
  const { title, body, image } = req.body;

  if (!title || !body || !image) {
    return res.status(422).json({ error: "All the fields are required!" });
  }

  req.user.password = undefined;
  const post = new Post({
    title,
    body,
    image: image,
    postedBy: req.user,
  });
  post
    .save()
    .then((result) => {
      res.json({ post: result });
    })
    .catch((err) => {
      res.status(500).json({ error: err.message });
    });
};

const userComments = async (req, res) => {
  const comment = {
    text: req.body.text,
    postedBy: req.user._id,
  };
  Post.findByIdAndUpdate(
    req.body.postId,
    {
      $push: { comments: comment },
    },
    {
      new: true,
    }
  )
    .populate("comments.postedBy", "_id name")
    .populate("postedBy", "_id name")
    .exec((err, result) => {
      if (err) {
        return res.status(422).json({ error: err });
      } else {
        res.json(result);
      }
    });
};

const likePost = async (req, res) => {
  Post.findByIdAndUpdate(
    req.body.postId,
    {
      $push: { likes: req.user._id },
    },
    {
      new: true,
    }
  )
    .populate("postedBy", "_id name")
    .exec((err, result) => {
      if (err) {
        return res.status(422).json({ error: err });
      } else {
        res.json(result);
      }
    });
};

const unlikePost = async (req, res) => {
  Post.findByIdAndUpdate(
    req.body.postId,
    {
      $pull: { likes: req.user._id },
    },
    {
      new: true,
    }
  )
    .populate("postedBy", "_id name")
    .exec((err, result) => {
      if (err) {
        return res.status(422).json({ error: err });
      } else {
        res.json(result);
      }
    });
};

const deletePost = async (req, res) => {
  Post.findOne({ _id: req.params.postId })
    .populate("postedBy", "_id")
    .exec((err, post) => {
      if (err || !post) {
        return res.status(422).json({ error: err });
      }
      if (post.postedBy._id.toString() === req.user._id.toString()) {
        post
          .remove()
          .then((result) => {
            res.json(result);
          })
          .catch((err) => {
            res.status(500).json({ error: err.message });
          });
      }
    });
};

module.exports = {
  createPost,
  getAllPost,
  getAllMyPost,
  userComments,
  likePost,
  unlikePost,
  deletePost,
};
