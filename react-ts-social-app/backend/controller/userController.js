const User = require("../models/user");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { JWT_SECRET } = require("../keys");

const registerUser = async (req, res) => {
  const { name, email, password } = req.body;

  if (!email || !password || !name) {
    return res.status(422).json({ error: "Please add all fields!" });
  }

  const isUserExists = await User.findOne({ email: email });

  if (!isUserExists) {
    const salt = await bcrypt.genSalt();
    const hashPassword = await bcrypt.hash(password, salt);

    const user = new User({
      name,
      email,
      password: hashPassword,
    });

    user.save((err, userData) => {
      if (err) {
        throw err;
      } else {
        res.json(userData);
      }
    });
  } else {
    return res
      .status(422)
      .json({ error: "user already exists with that email" });
  }
};

const loginUser = async (req, res) => {
  const { email, password } = req.body;

  if (!email || !password) {
    return res.status(422).json({ error: "Provide Credentials" });
  }
  User.findOne({ email: email }).then((savedUser) => {
    if (!savedUser) {
      return res.status(422).json({ error: "User not found" });
    }
    bcrypt.compare(password, savedUser.password).then((isMatch) => {
      if (isMatch) {
        const token = jwt.sign({ _id: savedUser._id }, JWT_SECRET);
        const { _id, name, email } = savedUser;
        res.json({ token, user: { _id, name, email } });
      } else {
        return res.status(422).json({ error: "Invalid email or password" });
      }
    });
  });
};

module.exports = { registerUser, loginUser };
