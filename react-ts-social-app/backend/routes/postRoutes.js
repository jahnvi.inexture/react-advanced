const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const auth = require("../middlewares/auth");
const PostController = require("../controller/postController");

router.get("/getallpost", auth, PostController.getAllPost);
router.get("/getmyposts", auth, PostController.getAllMyPost);
router.post("/createPost", auth, PostController.createPost);
router.put("/comment", auth, PostController.userComments);
router.put("/like", auth, PostController.likePost);
router.put("/unlike", auth, PostController.unlikePost);
router.delete("/deletepost/:id", auth, PostController.deletePost);

module.exports = router;
