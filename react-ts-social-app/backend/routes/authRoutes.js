const express = require("express");
const router = express.Router();
const UserController = require("../controller/userController");
const auth = require("../middlewares/auth");

router.post("/signup", UserController.registerUser);
router.post("/signin", UserController.loginUser);

module.exports = router;
