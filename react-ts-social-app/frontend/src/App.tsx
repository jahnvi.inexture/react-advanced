import React, { useEffect, useReducer, useContext } from "react";
import { BrowserRouter as Router, useHistory } from "react-router-dom";
import "./App.css";
import { ActionType } from "./context/action";
import { UserContext } from "./context/context";
import { userReducer } from "./context/reducer";
import { initialUserState } from "./context/state";
import Navbar from "./pages/AppLayOut/Navbar";
import Routes from "./route/MainRoute";

const Routing = () => {
  const history = useHistory();
  const { dispatch } = useContext(UserContext);

  useEffect(() => {
    const user = JSON.parse(localStorage.getItem("user") || "{}");
    if (user) {
      dispatch({ type: ActionType.USER, payload: user });
      history.push("/feed");
    } else {
      history.push("/login");
    }
  }, []);

  return (
    <>
      <Routes />
    </>
  );
};

function App() {
  const [state, dispatch] = useReducer(userReducer, initialUserState);

  return (
    <UserContext.Provider value={{ state, dispatch }}>
      <div className="App">
        <Router>
          <Navbar />
          <Routing />
        </Router>
      </div>
    </UserContext.Provider>
  );
}

export default App;

// user auth with jwt
// react redux saga
// post news feed like cmt notifications
// edit and delete
// notifications count
