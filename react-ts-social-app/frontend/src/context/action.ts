import { user, userState } from "./state";

export enum ActionType {
  USER = "USER",
  NO_USER = "NO_USER",
}

export interface userAction {
  type: ActionType.USER;
  payload: userState["users"];
}

// export interface noUserAction {
//   type: ActionType.NO_USER;
//   payload:
// }

export type userActioType = userAction;
