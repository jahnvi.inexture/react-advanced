export interface userState {
  users: user;
}

export interface user {
  _id: number;
  name: string;
  email: string;
  password: string;
}

export const initialUserState: userState = {
  users: {} as user,
};
