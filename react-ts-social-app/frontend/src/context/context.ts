import React, { createContext } from "react";
import { userActioType } from "./action";
import { initialUserState, userState } from "./state";

export const UserContext = createContext<{
  state: userState;
  dispatch: React.Dispatch<userActioType>;
}>({ state: initialUserState, dispatch: () => undefined });
