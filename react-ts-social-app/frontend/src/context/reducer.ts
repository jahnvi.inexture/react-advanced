import { ActionType, userActioType } from "./action";
import { initialUserState, userState } from "./state";

export function userReducer(state = initialUserState, action: userActioType) {
  switch (action.type) {
    case "USER":
      return {
        ...state,
        users: action.payload,
      };
    default:
      return state;
    // break;
  }
}

//helper function
// export const User = (User: user) => ({
//   type: ActionType.USER,
//   payload: User,
// });
