import { ActionTypes } from "./actionTypes";

interface signupUser {
  type: ActionTypes.SIGNUP_USER;
  payload: Object;
}

interface signupSuccess {
  type: ActionTypes.SIGNUP_SUCCESS;
  payload: Object;
}

interface apiError {
  type: ActionTypes.API_ERROR;
  payload: Object;
}

export type Action = signupUser | signupSuccess | apiError;
