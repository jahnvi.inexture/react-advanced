import { Action } from "./actions";
import { ActionTypes } from "../signup/actionTypes";

const initialState = {
  user: {},
  error: "",
  loading: false,
};

const signup = (state = initialState, action: Action) => {
  switch (action.type) {
    case ActionTypes.SIGNUP_USER:
      state = {
        ...state,
        loading: true,
      };
      break;
    case ActionTypes.SIGNUP_SUCCESS:
      state = {
        ...state,
        user: action.payload,
        loading: false,
      };
      break;
    case ActionTypes.API_ERROR:
      state = {
        ...state,
        // error: action.payload,
        loading: false,
      };
      break;
    default:
      state = { ...state };
  }
};

export default signup;
