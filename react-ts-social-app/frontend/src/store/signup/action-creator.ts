import { ActionTypes } from "./actionTypes";
import { Action } from "./actions";
import { Dispatch } from "redux";

export const signup = (user: Object, callback: Object) => {
  return (dispatch: Dispatch<Action>) => {
    dispatch({
      type: ActionTypes.SIGNUP_USER,
      payload: { user, callback },
    });
  };
};

export const signupSuccess = (user: Object) => {
  return (dispatch: Dispatch<Action>) => {
    dispatch({
      type: ActionTypes.SIGNUP_SUCCESS,
      payload: user,
    });
  };
};

export const apiError = (error: string) => {
  return (dispatch: Dispatch) => {};
};
