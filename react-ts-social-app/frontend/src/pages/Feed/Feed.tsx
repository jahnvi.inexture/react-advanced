import React, { useState, useEffect, useContext } from "react";
import { UserContext } from "../../context/context";

export interface dataState {
  data: {
    _id: number;
    body: String | number;
    title: String | number;
    image: string;
    likes: any;
    comments: any;
    postedBy: {
      _id: number;
      name: String;
    };
  };
}

const Dashboard: React.FC = () => {
  const [data, setData] = useState<dataState["data"][]>([]);
  const { state } = useContext(UserContext);

  useEffect(() => {
    fetch("http://localhost:8000/getallpost", {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("jwt"),
      },
    })
      .then((res) => res.json())
      .then((result) => {
        console.log(result);
        setData(result.posts);
      });
  }, []);

  const likePost = (id: number) => {
    fetch("http://localhost:8000/like", {
      method: "put",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("jwt"),
      },
      body: JSON.stringify({
        postId: id,
      }),
    })
      .then((res) => res.json())
      .then((result) => {
        // console.log(result);
        console.log("id error", result._id);
        const newData = data.map((count) => {
          if (count._id === result._id) {
            return result;
          } else {
            return count;
          }
        });
        setData(newData);
      });
  };

  const unlikePost = (id: number) => {
    fetch("http://localhost:8000/unlike", {
      method: "put",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("jwt"),
      },
      body: JSON.stringify({
        postId: id,
      }),
    })
      .then((res) => res.json())
      .then((result) => {
        // console.log(result);
        const newData = data.map((count) => {
          if (count._id === result._id) {
            return result;
          } else {
            return count;
          }
        });
        setData(newData);
      });
  };

  const postComment = (text: string, id: number) => {
    fetch("http://localhost:8000/comment", {
      method: "put",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("jwt"),
      },
      body: JSON.stringify({
        postId: id,
        text,
      }),
    })
      .then((res) => res.json())
      .then((result) => {
        // console.log(result);
        console.log("id error", result._id);
        const newData = data.map((comment) => {
          if (comment._id === result._id) {
            return result;
          } else {
            return comment;
          }
        });
        setData(newData);
      });
  };

  const deletePost = (id: number) => {
    fetch(`http://localhost:8000/deletepost/${id}`, {
      method: "delete",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("jwt"),
      },
    })
      .then((res) => res.json())
      .then((result) => {
        console.log(result);
        const newData = data.filter((posts) => {
          return posts._id !== result._id;
        });
        setData(newData);
      });
  };

  return (
    <div className="feed">
      {data?.map((post) => {
        return (
          <div className="card feed-card">
            <h5>
              {post.postedBy.name}
              {post.postedBy._id === state.users._id && (
                <i
                  className="material-icons"
                  onClick={() => deletePost(post._id)}
                  style={{
                    cursor: "pointer",
                    float: "right",
                    marginTop: "6px",
                  }}
                >
                  delete
                </i>
              )}
            </h5>
            <div className="card-image">
              <img src={post.image} alt="post_picture" />
            </div>
            <div className="card-content">
              {/* -----------likes----------- */}
              {/* {console.log("hlw", state.users._id)} */}
              {post.likes.includes(state.users._id) ? (
                <i
                  className="material-icons"
                  onClick={() => unlikePost(post._id)}
                  style={{ cursor: "pointer" }}
                >
                  thumb_down
                </i>
              ) : (
                <i
                  className="material-icons"
                  onClick={() => likePost(post._id)}
                  style={{ cursor: "pointer" }}
                >
                  thumb_up
                </i>
              )}

              <h6>{post.likes.length} likes</h6>
              <h6>{post.title}</h6>

              {/* ---------Comments--------- */}
              <p className="card-p">{post.body}</p>
              {post.comments.map((record: any) => {
                return (
                  <h6 key={record._id}>
                    <span style={{ fontWeight: "bold" }}>
                      {record.postedBy.name}
                    </span>{" "}
                    {record.text}
                  </h6>
                );
              })}
              <form
                onSubmit={(e: React.SyntheticEvent) => {
                  e.preventDefault();
                  const target = e.target as typeof e.target & {
                    text: { value: string };
                  };
                  const text = target.text.value; // typechecks!
                  postComment(text, post._id);
                }}
              >
                <input type="text" placeholder="add a comment" name="text" />
              </form>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default Dashboard;
