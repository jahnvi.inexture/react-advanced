import React, { useContext } from "react";
import insta from "../../assets/images/instagram.svg";
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";
import { UserContext } from "../../context/context";

const Navbar = () => {
  const { state } = useContext(UserContext);
  const history = useHistory();

  const navlinks = () => {
    if (state.users) {
      return [
        <li>
          <Link to="/createpost">
            <i
              className="material-icons"
              style={{ fontSize: "36px", marginTop: "3px" }}
            >
              add_to_photos
            </i>
          </Link>
        </li>,
        <li>
          <Link to="/notifications">
            <i
              className="material-icons"
              style={{ fontSize: "36px", marginTop: "3px" }}
            >
              favorite_border
            </i>
          </Link>
        </li>,
        <li>
          <button
            className="btn waves-effect waves-light #2196f3 blue darken-4"
            onClick={logoutUser}
          >
            Logout
          </button>
        </li>,
      ];
    } else {
      return [
        <li>
          <Link to="/login">
            <b>Login</b>
          </Link>
        </li>,
        <li>
          <Link to="/">
            <b>Signup</b>
          </Link>
        </li>,
      ];
    }
  };

  const logoutUser = () => {
    localStorage.clear();
    history.push("/login");
  };
  return (
    <div>
      <nav>
        <div className="nav-wrapper  white">
          {/* <Link to="/feed" className="brand-logo"> */}
          <img src={insta} alt="Logo" style={{ width: "7%", margin: "20px" }} />
          {/* </Link> */}
          <ul id="nav-mobile" className="right hide-on-med-and-down">
            {navlinks}
          </ul>
        </div>
      </nav>
    </div>
  );
};

export default Navbar;
