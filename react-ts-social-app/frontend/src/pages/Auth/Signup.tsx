import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import M from "materialize-css";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import SignupSchema from "../../validations/SignupSchema";

const Signup = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const history = useHistory();
  const {
    register,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(SignupSchema),
    reValidateMode: "onChange",
    mode: "all",
  });

  const registerUser = () => {
    fetch("http://localhost:8000/signup", {
      method: "post",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name,
        email,
        password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        // console.log(data);
        if (data.error) {
          M.toast({ html: data.error, classes: "c62828 red darken-3" });
        } else {
          M.toast({
            html: "Registered Successfully",
            classes: "43a047 green darken-1",
          });
          history.push("/login");
        }
      });
  };

  return (
    <div className="mycard">
      <div className="card auth-card input-field">
        <div className="card-content ">
          <h4 className="auth-head">Instagram</h4>
          <input
            type="text"
            placeholder="username"
            {...register("name")}
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
          <p className="error">{errors?.name?.message}</p>
          <input
            type="text"
            placeholder="email"
            value={email}
            {...register("email")}
            onChange={(e) => setEmail(e.target.value)}
          />
          <p className="error">{errors?.email?.message}</p>
          <input
            type="text"
            placeholder="password"
            value={password}
            {...register("password")}
            onChange={(e) => setPassword(e.target.value)}
          />
          <p className="error">{errors?.password?.message}</p>
          <button
            className="btn waves-effect waves-light #2196f3 blue darken"
            onClick={registerUser}
          >
            Signup
          </button>
          <p className="auth-link">
            Already registered{" "}
            <Link to="/login">
              <b>Login?</b>
            </Link>
          </p>
        </div>
      </div>
    </div>
  );
};

export default Signup;
