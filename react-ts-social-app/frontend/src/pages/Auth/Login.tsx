import React, { useState, useContext } from "react";
import { Link, useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import SignupSchema from "../../validations/SignupSchema";
import { UserContext } from "../../context/context";
import { ActionType } from "../../context/action";

const Login = () => {
  const { dispatch } = useContext(UserContext);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const history = useHistory();
  const {
    register,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(SignupSchema),
    reValidateMode: "onChange",
    mode: "all",
  });

  const loginUser = () => {
    fetch("http://localhost:8000/signin", {
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("jwt"),
      },
      body: JSON.stringify({
        email,
        password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data.error) {
          M.toast({ html: data.error, classes: "c62828 red darken-3" });
        } else {
          localStorage.setItem("jwt", data.token);
          localStorage.setItem("user", JSON.stringify(data.user));
          dispatch({ type: ActionType.USER, payload: data.user });
          M.toast({
            html: "Login Successfully",
            classes: "43a047 green darken-1",
          });
          history.push("/feed");
        }
      });
  };

  return (
    <div className="mycard">
      <div className="card auth-card input-field">
        <div className="card-content ">
          <h4 className="auth-head">Instagram</h4>
          <input
            type="text"
            placeholder="email"
            {...register("email")}
            onChange={(e) => setEmail(e.target.value)}
          />
          <p className="error">{errors?.email?.message}</p>
          <input
            type="text"
            placeholder="password"
            {...register("password")}
            onChange={(e) => setPassword(e.target.value)}
          />
          <p className="error">{errors?.password?.message}</p>
          <button
            className="btn waves-effect waves-light #2196f3 blue darken"
            onClick={loginUser}
          >
            Login
          </button>
          <p className="auth-link">
            Don't have an account{" "}
            <Link to="/">
              <b>Signup?</b>
            </Link>
          </p>
        </div>
      </div>
    </div>
  );
};

export default Login;
