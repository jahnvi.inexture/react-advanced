import React from "react";

const Notifications = () => {
  return (
    <div className="center-align notification">
      <h5>notifications</h5>
      <div className="card center-align">
        <div className="divider"></div>
        <div className="section">
          <h5>Section 1</h5>
          <p>Stuff</p>
        </div>
        <div className="divider"></div>
        <div className="section">
          <h5>Section 2</h5>
          <p>Stuff</p>
        </div>
        <div className="divider"></div>
        <div className="section">
          <h5>Section 3</h5>
          <p>Stuff</p>
        </div>
      </div>
    </div>
  );
};

export default Notifications;
