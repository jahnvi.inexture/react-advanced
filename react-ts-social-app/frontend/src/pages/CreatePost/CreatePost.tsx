import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import M from "materialize-css";

const CreatePost = () => {
  const [title, setTitle] = useState("");
  const [body, setBody] = useState("");
  const [image, setImage] = useState<File>();
  const [url, setUrl] = useState<File>();
  const history = useHistory();

  useEffect(() => {
    if (url) {
      fetch("http://localhost:8000/createpost", {
        method: "post",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + localStorage.getItem("jwt"),
        },
        body: JSON.stringify({
          title,
          body,
          image: url,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          if (data.error) {
            M.toast({ html: data.error, classes: "c62828 red darken-3" });
          } else {
            M.toast({
              html: "Post Uploaded Successfully!",
              classes: "43a047 green darken-1",
            });
            history.push("/feed");
          }
        });
    }
  }, [url]);

  const handleSelectPost = (e: React.ChangeEvent<HTMLInputElement>) => {
    const postImage = e.target.files;
    if (!postImage) return;
    setImage(postImage[0]);
  };

  const postDetails = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>) => {
    if (image) {
      const data = new FormData();
      data.append("file", image);
      data.append("upload_preset", "social-app");
      data.append("cloud_name", "jahnviks");
      fetch("https://api.cloudinary.com/v1_1/jahnviks/image/upload", {
        method: "post",
        body: data,
      })
        .then((res) => res.json())
        .then((data) => {
          setUrl(data.url);
          // console.log(data);
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  return (
    <div className="card input-field card-post">
      <input
        type="text"
        placeholder="title"
        value={title}
        onChange={(e) => setTitle(e.target.value)}
      />
      <input
        type="text"
        placeholder="Write a caption"
        value={body}
        onChange={(e) => setBody(e.target.value)}
      />
      <div className="file-field input-field">
        <div className="btn  #2196f3 blue darken">
          <span className="post-file">Upload Image</span>
          <input type="file" onChange={handleSelectPost} />
        </div>
        <div className="file-path-wrapper">
          <input className="file-path validate" type="text" />
        </div>
      </div>
      <button
        className="btn waves-effect waves-light #2196f3 blue darken"
        onClick={postDetails}
      >
        Add Post
      </button>
    </div>
  );
};

export default CreatePost;
