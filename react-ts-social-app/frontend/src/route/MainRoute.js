import React from "react";
import { Route, Switch } from "react-router-dom";
import PrivateRoute from "../components/PrivateRoutes";
import Login from "../pages/Auth/Login";
import Signup from "../pages/Auth/Signup";
import CreatePost from "../pages/CreatePost/CreatePost";
import Feed from "../pages/Feed/Feed";
import Notifications from "../pages/Notifications/Notifications";
import Profile from "../pages/Profile/Profile";

const Routes = () => {
  return (
    <div>
      <Switch>
        {/* Public Routes */}
        <Route exact path="/" component={Signup} />
        <Route exact path="/login" component={Login} />

        {/* Private Routes */}
        <PrivateRoute exact path="/feed" component={Feed} />
        <PrivateRoute exact path="/profile" component={Profile} />
        <PrivateRoute exact path="/createpost" component={CreatePost} />
        <PrivateRoute exact path="/notifications" component={Notifications} />
      </Switch>
    </div>
  );
};

export default Routes;
