import * as yup from "yup";

const SignupSchema = yup.object().shape({
  email: yup.string().required().email(),
  name: yup.string().required().min(3).max(25),
  password: yup.string().required().min(4).max(120),
});

export default SignupSchema;
