import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import "./index.css";
import App from "./App";
import configureStore from "./store";

let initialState = {};
try {
  initialState = JSON.parse(localStorage.getItem("state")) || {};
} catch (e) {
  // console.error('localStorage get error:', e);
}
const store = configureStore(initialState);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
