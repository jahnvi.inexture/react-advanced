import React from "react";
import Header from "./Header";
import Footer from "./Footer";

export default function AppLayOut(props) {
  return (
    <>
      <Header />
      <div>{props.children}</div>
      <Footer />
    </>
  );
}
