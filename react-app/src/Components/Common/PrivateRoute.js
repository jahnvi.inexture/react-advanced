import React, { useState } from "react";
import { Route, Redirect } from "react-router-dom";
import AppLayout from "../../pages/AppLayout";

export default function PrivateRoute({ component: Component, ...rest }) {
  const [token] = useState(localStorage.getItem("token"));
  return token ? (
    <AppLayout>
      <Route {...rest} render={(props) => <Component {...props} />} />
    </AppLayout>
  ) : (
    <Redirect
      to={{
        pathname: "/login",
      }}
    />
  );
}
