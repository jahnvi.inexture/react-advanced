import React from "react";
import { Route, Switch } from "react-router-dom";
import PrivateRoute from "../Components/Common/PrivateRoute";
import ForgotPassword from "../pages/Auth/ForgotPassword";
import Login from "../pages/Auth/Login";
import ResetPassword from "../pages/Auth/ResetPassword";

export const Routes = () => {
  return (
    <div>
      <Switch>
        {/* Public Routes  */}
        <Route exact path="/login" component={Login} />
        <Route exact path="/forgot-password" component={ForgotPassword} />
        <Route exact path="/reset-password" component={ResetPassword} />

        {/* Private Routes  */}
        <PrivateRoute exact path="/" />
      </Switch>
    </div>
  );
};

export default Routes;
