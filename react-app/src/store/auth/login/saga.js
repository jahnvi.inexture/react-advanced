import { call, put, takeEvery } from "redux-saga/effects";

// Login Redux States
import { LOGIN_USER, LOGOUT_USER } from "./actionTypes";
import { apiError, loginSuccess, logoutUserSuccess } from "./actions";

import { loginService } from "../../../Services/AuthService";

function* loginUser({ payload: { user, callback } }) {
  try {
    const response = yield call(loginService, user);
    if (response?.token) {
      yield put(loginSuccess(response));
      localStorage.setItem("token", response?.token);
      callback(response);
    }
  } catch (error) {
    yield put(apiError(error));
  }
}

function* logoutUser({ payload: { history } }) {
  try {
    localStorage.clear();
    history.push("/login");
    yield put(logoutUserSuccess());
  } catch (error) {
    yield put(apiError(error));
  }
}

function* authSaga() {
  yield takeEvery(LOGIN_USER, loginUser);
  yield takeEvery(LOGOUT_USER, logoutUser);
}

export default authSaga;
