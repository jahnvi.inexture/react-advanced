import { combineReducers } from "redux";
import { LOGOUT_USER } from "./auth/login/actionTypes";

// Authentication
import Login from "./auth/login/reducer";

const appReducer = combineReducers({
  Login,
});

const rootReducer = (state, action) => {
  if (action.type === LOGOUT_USER) {
    state = undefined;
  }

  return appReducer(state, action);
};

export default rootReducer;
