import { createStore, applyMiddleware, compose } from "redux";
import createSagaMiddleware from "redux-saga";

// import localForage from "localforage"
import rootReducer from "./reducers";
import rootSaga from "./sagas";

export default function configureStore(initialState) {
  const sagaMiddleware = createSagaMiddleware();
  const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  const logger = (store) => (next) => (action) => {
    const result = next(action);

    try {
      const fullState = store.getState();
      localStorage.setItem("state", JSON.stringify(fullState));
    } catch (e) {
      // console.error('sessionStorage set error:', e);
    }
    return result;
  };

  const store = createStore(
    rootReducer,
    initialState,
    composeEnhancers(applyMiddleware(sagaMiddleware, logger))
  );
  sagaMiddleware.run(rootSaga);

  return store;
}
