import { gql } from "@apollo/client";

export const LOAD_USERS = gql`
  query {
    getAllUsers {
      id
      firstName
      lastName
      email
      password
    }
  }
`;

export const USER = gql`
  query ($id: Int) {
    getUser(id: $id) {
      id
      firstName
      lastName
      email
      password
    }
  }
`;
