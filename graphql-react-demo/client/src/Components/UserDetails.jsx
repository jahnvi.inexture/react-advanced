import React, { useState, useEffect } from "react";
import { useQuery, graphql } from "@apollo/client";
import { USER } from "../GraphQL/Queries";

const UserDetails = ({ userId }) => {
  const { data } = useQuery(USER, { variables: { userId } });
  const [user, setUser] = useState();

  useEffect(() => {
    if (data) {
      setUser(data.getUser);
    }
  }, [data]);
  console.log("data===>", data);

  console.log("get========>", userId);

  return <div>{/* <h4>User Details</h4> */}</div>;
};

export default UserDetails;
