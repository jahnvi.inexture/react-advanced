import React, { useEffect, useState } from "react";
import { useQuery, gql, useMutation } from "@apollo/client";
import { LOAD_USERS } from "../GraphQL/Queries";
import { DELETE_USER } from "../GraphQL/Mutations";
import UserDetails from "./UserDetails";

const GetUsers = ({ userId }) => {
  const { error, loading, data } = useQuery(LOAD_USERS);
  const [users, setUsers] = useState([]);
  const [selected, setSelected] = useState(false);
  const [deleteUser, { loading: deleting, error: deleteError }] =
    useMutation(DELETE_USER);

  useEffect(() => {
    console.log("data===>", data);
    if (data) {
      setUsers(data.getAllUsers);
    }
  }, [data]);

  const remove = () => {
    if (deleting) return;
    deleteUser({
      variables: { id: userId },
    });
  };

  return (
    <div>
      {users.map((user) => {
        return (
          <>
            <h4
              style={{ cursor: "pointer" }}
              key={user.id}
              onClick={(e) => setSelected({ selected: user.id })}
            >
              {user.id} : {user.firstName} {user.lastName}
            </h4>
            <button onClick={remove}>delete</button>
          </>
        );
      })}
      <UserDetails userId={selected} />
    </div>
  );
};

export default GetUsers;
