import { useMutation } from "@apollo/client";
import React, { useState } from "react";
import { CREATE_USER_MUTATION } from "../GraphQL/Mutations";
import styled from "styled-components";
import { LOAD_USERS } from "../GraphQL/Queries";

const Container = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 4%;
`;

const Label = styled.label`
  font-weight: bold;
  display: block;
`;

const Input = styled.input`
  width: 30vh;
`;

const Form = () => {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [createUser, { error }] = useMutation(CREATE_USER_MUTATION);

  const addUser = () => {
    createUser({
      variables: {
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: password,
      },
      refetchQueries: [{ query: LOAD_USERS }],
    });

    alert("User added successfully");
    if (error) {
      console.log(error);
    }
  };

  return (
    <Container>
      <form>
        <div class="form-group">
          <Label style={{ fontWeight: "bold" }}>First Name</Label>
          <Input type="text" onChange={(e) => setFirstName(e.target.value)} />
        </div>
        <div class="form-group">
          <Label style={{ fontWeight: "bold" }}>Last Name</Label>
          <Input type="text" onChange={(e) => setLastName(e.target.value)} />
        </div>
        <div class="form-group">
          <Label style={{ fontWeight: "bold" }}>Email address</Label>
          <Input type="email" onChange={(e) => setEmail(e.target.value)} />
        </div>
        <div class="form-group">
          <Label style={{ fontWeight: "bold" }}>Password</Label>
          <Input
            type="password"
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <button
          style={{ marginTop: "15px" }}
          type="submit"
          class="btn btn-primary"
          onClick={addUser}
        >
          Submit
        </button>
      </form>
    </Container>
  );
};

export default Form;
