const graphql = require("graphql");
const {
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLInt,
  GraphQLString,
  GraphQLList,
} = graphql;

const userData = require("../MOCK_DATA.json");

const UserType = require("./TypeDefs/UserType");

const RootQuery = new GraphQLObjectType({
  name: "Query",
  description: "Root Query",
  fields: () => ({
    getAllUsers: {
      type: new GraphQLList(UserType),
      description: "All Users",
      resolve: () => userData,
    },
    getUser: {
      type: UserType,
      description: "A Single User",
      args: {
        id: { type: GraphQLInt },
      },
      resolve: (parent, args) =>
        userData.find((getUser) => getUser.id === args.id),
    },
  }),
});

const RootMutation = new GraphQLObjectType({
  name: "Mutation",
  description: "Root Mutation",
  fields: {
    createUser: {
      type: UserType,
      description: "Create a user",
      args: {
        firstName: { type: GraphQLString },
        lastName: { type: GraphQLString },
        email: { type: GraphQLString },
        password: { type: GraphQLString },
      },
      resolve(parent, args) {
        userData.push({
          id: userData.length + 1,
          firstName: args.firstName,
          lastName: args.lastName,
          email: args.email,
          password: args.password,
        });
        return args;
      },
    },
    deleteUser: {
      type: UserType,
      description: "Delete Users By ID",
      args: {
        id: { type: GraphQLInt },
      },
      resolve(parent, args) {
        const userDeleted = userData.pop({
          id: args.id,
        });
        return userDeleted;
      },
    },
  },
});

module.exports = new GraphQLSchema({
  query: RootQuery,
  mutation: RootMutation,
});
