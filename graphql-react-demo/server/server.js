const express = require("express");
const app = express();
const expressGraphQL = require("express-graphql").graphqlHTTP;
const schema = require("./Schemas/index");
const cors = require("cors");

app.use(cors());
app.use(express.json());
app.use(
  "/graphql",
  expressGraphQL({
    schema,
    graphiql: true,
  })
);

const PORT = 5000;

app.listen(PORT, () => {
  console.log(`server is listening on port ${PORT}`);
});
