import React from "react";
// import { addDecorator } from "@storybook/react";
import { ThemeProvider, theme, CSSReset, Box } from "@chakra-ui/core";
// import { withConsole } from "@storybook/addon-console";
// import { withA11y } from "@storybook/addon-a11y";

// addDecorator((story) => (
//   <ThemeProvider theme={theme}>
//     <CSSReset />
//     <Box m="4">{story()}</Box>
//   </ThemeProvider>
// ));

// addDecorator((storyFn, context) => withConsole()(storyFn)(context));
// addDecorator(withA11y);

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
  options: {
    storySort: (a, b) =>
      a[1].kind === b[1].kind
        ? 0
        : a[1].id.localeCompare(b[1].id, undefined, { numeric: true }),
  },
};

export const decorators = [
  (Story) => (
    <ThemeProvider theme={theme}>
      <CSSReset />
      <Box>
        <Story />
      </Box>
    </ThemeProvider>
  ),
];
