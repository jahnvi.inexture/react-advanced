import React from "react";
import { Button } from "@chakra-ui/core";
import { action, actions } from "@storybook/addon-actions";
import { text, boolean } from "@storybook/addon-knobs";

export default {
  title: "Chakra/Button",
  component: Button,
  argTypes: {
    onClick: { action: "clicked" },
  },
};

export const Yellow = () => (
  <Button onClick={action("Click handler")} variantColor="yellow">
    Yellow
  </Button>
);

export const Purple = () => (
  <Button {...actions("onClick", "onMouseOver")} variantColor="purple">
    Purple
  </Button>
);

export const Console = () => (
  <Button
    onClick={() => console.log("Button Clicked", process.env.STORYBOOK_THEME)}
    variantColor="blue"
  >
    Black
  </Button>
);

const Template = (args) => <Button {...args} />;

// export const Blue = Template.bind({});
// Blue.args = {
//   varientColor: "green",
//   children: "Blue",
// };

export const Teal = Template.bind({});
Teal.args = {
  varientColor: "teal",
  children: "Teal",
};

// knobs allows us to edit props dynamically
export const Knobs = () => (
  <Button variantColor="pink" disabled={boolean("Disabled", false)}>
    {text("Label", "Button Label")}
  </Button>
);
