import React from "react";
import "./Table.css";
import RenderTableData from "./RenderTableData";

const Table = ({ data }) => {
  return (
    <div>
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Registered Course</th>
          </tr>
        </thead>
        <tbody>
          {data ? (
            <RenderTableData data={data} />
          ) : (
            <tr>
              <td>No student data available</td>
              <td>No student data available</td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  );
};

export default Table;
