import React from "react";
import Table from "./Table";
import { data } from "../../data";
import { withKnobs, object } from "@storybook/addon-knobs";

export default {
  title: "Table Component",
  component: Table,
  decorators: [withKnobs],
};

export function ShowStudentsData() {
  return <Table data={object("data", data)} />;
}

export function EmptyData() {
  return <Table />;
}
