import React from "react";

const RenderTableData = ({ data }) => {
  return (
    <>
      {data.map((student) => (
        <tr>
          <td>{student.name}</td>
          <td>{student.course}</td>
        </tr>
      ))}
    </>
  );
};

export default RenderTableData;
