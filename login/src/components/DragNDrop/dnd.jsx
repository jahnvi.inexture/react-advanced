import React from "react";
import Board from "@lourenci/react-kanban";
import { board } from "./data";

const dnd = () => {
  return (
    <div>
      <Board
        allowAddColumn
        allowRemoveColumn
        allowRenameColumn
        allowRemoveCard
        onColumnRemove={console.log}
        onCardRemove={console.log}
        onColumnRename={console.log}
        initialBoard={board}
        allowAddCard={{ on: "top" }}
        onNewCardConfirm={(draftCard) => ({
          id: new Date().getTime(),
          ...draftCard,
        })}
        onCardNew={console.log}
      />
    </div>
  );
};

export default dnd;
