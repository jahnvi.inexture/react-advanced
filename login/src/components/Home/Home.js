import React from "react";
// import AuthContext from "../../context/auth-context";

import Card from "../UI/Card/Card";
import classes from "./Home.module.css";
import { Fragment } from "react/cjs/react.production.min";
import { Link, useHistory } from "react-router-dom";

const Home = (props) => {
  // const authCtx = useContext(AuthContext);

  const history = useHistory();
  const handleClick = () => {
    let XO = window.prompt("Choose X or O: ");
    alert("You've Choose " + XO);
    history.push(`/tic-tac-toe/${XO}`);
  };

  return (
    <Fragment>
      <Card className={classes.home}>
        <h1>Welcome back!</h1>
        {/* <Button onClick={authCtx.onLogout}>Logout</Button> */}
        <div className="link" onClick={handleClick}>
          Play tic tac toe
        </div>
        <Link className="link" to="/Jeopardy">
          Play Jeopardy
        </Link>
        <Link className="link" to="/dnd">
          React-dnd
        </Link>
      </Card>
    </Fragment>
  );
};

export default Home;
