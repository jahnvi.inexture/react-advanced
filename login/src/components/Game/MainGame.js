import React, { useState } from "react";
import { Link, useParams } from "react-router-dom";
import { calculateWinner } from "../../helper/helper";
import Button from "../UI/Button/Button";
import Board from "./Board";

const style = {
  display: "block",
  margin: "15px",
};

const MainGame = (props) => {
  const [board, setBoard] = useState(Array(9).fill(null));
  const [choosePlayer, setChoosePlayer] = useState(true);
  const winner = calculateWinner(board);
  const params = useParams();
  const XO = params.XO;
  const opponent = XO === "X" ? "O" : "X";

  const handleClick = (i) => {
    const boardCopy = [...board];
    if (winner || boardCopy[i]) {
      return;
    }
    boardCopy[i] = choosePlayer ? XO : opponent;

    setBoard(boardCopy);
    setChoosePlayer(!choosePlayer);
  };

  const renderMoves = () => {
    return (
      <div>
        <Button onClick={() => setBoard(Array(9).fill(null))}>
          Reset Game
        </Button>
        <Link style={style} to="/home">
          Back
        </Link>
      </div>
    );
  };

  return (
    <>
      <h1 style={{ textAlign: "center" }}>React Tic Tac Toe</h1>
      <Board squares={board} onClick={handleClick} />
      <div style={{ textAlign: "center" }}>
        <h2 style={{ textAlign: "center" }}>
          {winner
            ? `Winner: ` + winner
            : `Next Player: ` + (choosePlayer ? XO : opponent)}
        </h2>
        {renderMoves()}
      </div>
    </>
  );
};

export default MainGame;
