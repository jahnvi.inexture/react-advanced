import { useEffect, useState } from "react";
import Question from "../Question/Question";

const Categories = ({
  value,
  id,
  changeClueValue,
  displayClue,
  pages,
  markQ,
}) => {
  const [data, setData] = useState();
  useEffect(() => {
    async function fetchData() {
      const res = await fetch(
        `https://jservice.io/api/clues?category=${id}&offset=${pages}`
      );

      if (!res.ok) {
        throw Error("Something went wrong");
      }
      const data = await res.json();
      setData(data);
    }

    fetchData();
  }, [pages, id]);

  return (
    <>
      <div>
        <div className="Board_money">{value}</div>
        {data && (
          <div>
            {data.map((i) => {
              return (
                <Question
                  markQ2={markQ}
                  onClueClick={changeClueValue}
                  data={i}
                  clue={displayClue}
                  key={i.id}
                />
              );
            })}
          </div>
        )}
      </div>
    </>
  );
};

export default Categories;
