import React, { useState, useEffect } from "react";
import ControlPagination from "../ControlPagination/ControlPagination";
import Categories from "../Categories/Categories";
import LoadingSpinner from "../../UI/LoadingSpinner/LoadingSpinner";

const GameBoard = (props) => {
  const [clueCard, setClueCard] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [data, setData] = useState([]);
  const [page, setPage] = useState(0);
  const [cat, setCat] = useState(5);

  const changeClueCard = () => {
    setClueCard((prevState) => !prevState);
    // setClueCard(true);
  };

  useEffect(() => {
    async function fetchData() {
      const res = await fetch(
        `https://jservice.io/api/categories?count=${cat}&offset=8`
      );

      if (!res.ok) {
        throw Error("Something went wrong");
      }
      const data = await res.json();
      setData(data);
      setIsLoading(true);
    }

    setTimeout(() => {
      setIsLoading(false);
    }, 2000);

    fetchData();
  }, [cat]);

  const changePageController = (pages) => {
    setPage(pages);
  };

  const changeCatController = (categories) => {
    setCat(categories);
  };

  return (
    <>
      <h1
        style={{
          paddingBottom: "1rem",
          textAlign: "center",
        }}
      >
        React Jeopardy
      </h1>
      <div className="Board">
        {isLoading ? (
          <LoadingSpinner />
        ) : (
          data.map((i) => {
            return (
              <Categories
                pages={page}
                categories={cat}
                key={i.id}
                markQ={props.markQ}
                displayClue={clueCard}
                changeClueValue={changeClueCard}
                value={i.title}
                id={i.id}
              />
            );
          })
        )}
      </div>
      <div>
        <ControlPagination
          changeCat={changeCatController}
          changePage={changePageController}
        />
      </div>
    </>
  );
};

export default GameBoard;
