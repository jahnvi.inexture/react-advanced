import Clue from "../Clue/Clue";
import { useState } from "react";

const Question = (props) => {
  const [markQ, setMarkQ] = useState(false);
  const [popupState, setPopupState] = useState(false);

  const changeMarkQ = () => {
    setMarkQ(true);
    props.markQ2();
    console.log("markQ========>" + markQ);
  };

  console.log("markques======>" + markQ);

  return (
    <>
      <div onClick={() => setPopupState(true)}>
        <div
          onClick={props.onClueClick}
          className={!markQ ? "Board_money" : "text-hidden"}
        >
          {props.data.value}
        </div>
        <div>
          {props.clue && popupState === true && !markQ && (
            <Clue
              onClick={() => setPopupState(false)}
              markQuestions={changeMarkQ}
              question={props.data.question}
              answer={props.data.answer}
            />
          )}
        </div>
      </div>
    </>
  );
};

export default Question;
