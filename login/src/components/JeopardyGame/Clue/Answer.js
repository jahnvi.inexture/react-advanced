import { Card } from "react-bootstrap";
import classes from "../Clue/Clue.module.css";
import CustButton from "../../UI/Button/Button";
import CustCard from "../../UI/Card/Card";

const Answer = ({ onClick, clue }) => {
  return (
    <div className="answer">
      <Card className="text-center">
        <CustCard className={classes.card}>
          <Card.Title>Answer</Card.Title>

          <Card.Text>{clue}</Card.Text>

          <CustButton className={classes.btn} onClick={onClick}>
            Clue
          </CustButton>
        </CustCard>
      </Card>
    </div>
  );
};

export default Answer;
