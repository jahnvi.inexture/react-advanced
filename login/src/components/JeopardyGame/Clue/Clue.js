import React, { useState } from "react";
import { Card } from "react-bootstrap";
import classes from "../Clue/Clue.module.css";
import CustButton from "../../UI/Button/Button";
import CustCard from "../../UI/Card/Card";
import Answer from "../Clue/Answer";

const Clue = ({ question, answer, onClick, markQuestions }) => {
  const [asnwerCard, setAnswerCard] = useState(false);

  const toggleAnswer = () => {
    setAnswerCard((prevState) => !prevState);
    // setAnswerCard(true);
  };

  return (
    <>
      <div className="Overlay">
        <Card className="text-center">
          <CustCard className={classes.card} className="Popup">
            <Card.Title>Clue</Card.Title>
            <Card.Text>{question}</Card.Text>
            <div onClick={toggleAnswer}>
              <CustButton className={classes.btn}> Answer</CustButton>
            </div>
            <div onClick={markQuestions}>
              <CustButton onClick={onClick}>Done</CustButton>
            </div>

            {asnwerCard && (
              <Answer clue={answer} onClick={() => toggleAnswer()} />
            )}
          </CustCard>
        </Card>
      </div>
    </>
  );
};

export default Clue;
