import React, { useContext, useState, useEffect } from "react";
import { Route, Redirect } from "react-router-dom";

import Login from "./components/Login/Login";
import Home from "./components/Home/Home";
import MainHeader from "./components/MainHeader/MainHeader";
import AuthContext from "./context/auth-context";
import MainGame from "./components/Game/MainGame";
import GameBoard from "./components/JeopardyGame/GameBoard/GameBoard";
import CustButton from "./components/UI/Button/Button";
import dnd from "./components/DragNDrop/dnd";

function App() {
  const ctx = useContext(AuthContext);
  const [render, setRender] = useState(true);

  const [marked, setMarked] = useState(false);
  const getMarkQ = () => {
    setMarked(true);
  };

  const rendering = () => {
    setRender(!render);
    setMarked(false);
  };

  useEffect(() => {
    console.log("RESET========>>>>>>>>>", marked);
  }, [marked]);

  return (
    <>
      <MainHeader />
      <main>{!ctx.isLoggedIn && <Login />}</main>
      <Route path="/" exact>
        <Redirect to="/home" />
      </Route>
      {ctx.isLoggedIn && <Route path="/home" exact component={Home} />}
      <Route path="/tic-tac-toe/:XO" exact component={MainGame} />
      <Route path="/dnd" exact component={dnd} />
      <Route path="/Jeopardy" exact>
        <GameBoard key={render} markQ={getMarkQ} />
        <div className="reset_div">
          <CustButton
            className="reset_btn"
            disabled={!marked}
            onClick={() => rendering()}
          >
            RESET
          </CustButton>
        </div>
      </Route>
    </>
  );
}

export default App;
