export interface Clues {
  data: {
    id: number;
    question: string | number | boolean;
    answer: string | number | boolean;
    value: number;
  }[];
}
