import React, { useState, useEffect } from "react";
import LoadingSpinner from "../../UI/LoadingSpinner/LoadingSpinner";
import Categories from "../Categories/Categories";
import ControlPagination from "../ControlPagination/ControlPagination";
import classes from "../GameBoard/GameBoard.module.css";

export interface dataState {
  data: {
    categories: string;
    catCount: number;
    id: number;
    title: string;
    clues_count: number;
  }[];
}

type Props = {
  markQ: any;
};

const GameBoard: React.FC<Props> = ({ markQ }) => {
  const [clueCard, setClueCard] = useState(false);
  const [data, setData] = useState<dataState["data"]>([]);
  const [isLoading, setIsLoading] = useState(false);
  const [page, setPage] = useState(0);
  const [cat, setCat] = useState(5);

  const changeClueCard = () => {
    setClueCard((prevState) => !prevState);
    // setClueCard(true);
  };

  useEffect(() => {
    async function fetchData() {
      const res = await fetch(
        `https://jservice.io/api/categories?count=${cat}&offset=8`
      );

      if (!res.ok) {
        throw Error("Something Went Wrong");
      }
      const data = await res.json();
      setData(data);
      setIsLoading(true);
      console.log("GameBoard==========>", data);
    }

    setTimeout(() => {
      setIsLoading(false);
    }, 2000);

    fetchData();
  }, [cat]);

  const changePageController = (pages: number) => {
    setPage(pages);
  };

  const changeCatController = (categories: number) => {
    setCat(categories);
  };

  return (
    <>
      <h1
        style={{
          marginTop: "2rem",
          paddingBottom: "1rem",
          textAlign: "center",
        }}
      >
        React + TypeScript Jeopardy
      </h1>
      <div className={classes.Board}>
        {isLoading ? (
          <LoadingSpinner />
        ) : (
          data.map((i) => {
            return (
              <Categories
                pages={page}
                markQ={markQ}
                categories={cat}
                key={i.id}
                displayClue={clueCard}
                changeClueValue={changeClueCard}
                value={i.title}
                id={i.id}
              />
            );
          })
        )}
      </div>
      <div>
        <ControlPagination
          changeCat={changeCatController}
          changePage={changePageController}
        />
      </div>
    </>
  );
};

export default GameBoard;
