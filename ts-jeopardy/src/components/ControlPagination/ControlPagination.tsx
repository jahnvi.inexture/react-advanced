import { useState } from "react";
import { Dropdown, DropdownButton } from "react-bootstrap";

const ControlPagination: React.FC<{
  changeCat: (catCount: number) => void;
  changePage: (pageCount: number) => void;
}> = (props) => {
  const [pages, setPages] = useState(0);
  const [categories, setCategories] = useState(5);

  const pageHandler = (page: number) => {
    setPages(page);
  };

  const categoryHandler = (cat: number) => {
    setCategories(cat);
  };

  props.changeCat(categories);
  props.changePage(pages);

  return (
    <>
      <div className="dbtn">
        <DropdownButton title="Control Categories">
          <Dropdown.Item href="#/action-1" onClick={() => categoryHandler(2)}>
            2
          </Dropdown.Item>
          <Dropdown.Item href="#/action-2" onClick={() => categoryHandler(3)}>
            3
          </Dropdown.Item>
          <Dropdown.Item href="#/action-3" onClick={() => categoryHandler(5)}>
            5
          </Dropdown.Item>
        </DropdownButton>
        <DropdownButton title="Control Clues">
          <Dropdown.Item href="#/action-1" onClick={() => pageHandler(3)}>
            2
          </Dropdown.Item>
          <Dropdown.Item href="#/action-2" onClick={() => pageHandler(2)}>
            3
          </Dropdown.Item>
          <Dropdown.Item href="#/action-3" onClick={() => pageHandler(0)}>
            5
          </Dropdown.Item>
        </DropdownButton>
      </div>
    </>
  );
};

export default ControlPagination;
