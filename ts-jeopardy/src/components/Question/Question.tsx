import React, { useState } from "react";
import Clue from "../Clue/Clue";
import classes from "../GameBoard/GameBoard.module.css";
import { Clues } from "../../Data/CluesInterface";

type Props = {
  clue: String | Number | boolean;
  onClueClick: React.MouseEventHandler<HTMLDivElement>;
  money: number;
  dataClue: Clues["data"];
  markQ2: any;
};

const Question: React.FC<Props> = ({
  clue,
  onClueClick,
  dataClue,
  money,
  markQ2,
}) => {
  const [popupState, setPopupState] = useState(false);
  const [markQ, setMarkQ] = useState(false);

  const handleMarkQ = () => {
    setMarkQ(true);
    markQ2();
  };

  return (
    <>
      <div onClick={() => setPopupState(true)}>
        <div
          onClick={onClueClick}
          className={!markQ ? classes.Board_money : classes.text_hidden}
        >
          {money}
        </div>
        <div>
          {clue && popupState === true && !markQ && (
            <div>
              {dataClue.map((i) => {
                return (
                  <Clue
                    onClick={() => setPopupState(false)}
                    markQuestion={handleMarkQ}
                    question={i.question}
                    answer={i.answer}
                  />
                );
              })}
            </div>
          )}
        </div>
      </div>
    </>
  );
};

export default Question;
