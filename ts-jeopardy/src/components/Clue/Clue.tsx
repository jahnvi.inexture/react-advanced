import React, { useState } from "react";
import { Card } from "react-bootstrap";
import CustButton from "../../UI/Button/Button";
import CustCard from "../../UI/Card/Card";
import classes from "../Clue/Clue.module.css";
import Answer from "./Answer";

type Props = {
  onClick?: React.MouseEventHandler;
  question: any;
  answer: any;
  markQuestion: () => void;
};

const Clue: React.FC<Props> = ({ onClick, answer, question, markQuestion }) => {
  const [answerCard, setAnswerCard] = useState(false);

  const toggleAnswerCard = () => {
    setAnswerCard((prevState) => !prevState);
  };

  return (
    <>
      <div className={classes.Overlay}>
        <Card className="text-center">
          <CustCard className={classes.Popup}>
            <Card.Title>Clue</Card.Title>
            <Card.Text>{question}</Card.Text>
            <div onClick={toggleAnswerCard}>
              <CustButton className={classes.btn}>Answer</CustButton>
            </div>
            <div onClick={markQuestion}>
              <CustButton onClick={onClick}>Done</CustButton>
            </div>
            {answerCard && (
              <Answer clue={answer} onClick={() => toggleAnswerCard()} />
            )}
          </CustCard>
        </Card>
      </div>
    </>
  );
};

export default Clue;
