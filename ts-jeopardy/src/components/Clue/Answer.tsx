import React from "react";
import { Card } from "react-bootstrap";
import CustButton from "../../UI/Button/Button";
import CustCard from "../../UI/Card/Card";
import classes from "../Clue/Clue.module.css";

type Props = {
  onClick?: React.MouseEventHandler;
  clue?: String | Number | boolean;
};

const Answer: React.FC<Props> = ({ onClick, clue }) => {
  return (
    <div className={classes.answer}>
      <Card className="text-center">
        <CustCard className={classes.card}>
          <Card.Title>Answer</Card.Title>
          <Card.Text>{clue}</Card.Text>
          <CustButton className={classes.btn} onClick={onClick}>
            Clue
          </CustButton>
        </CustCard>
      </Card>
    </div>
  );
};

export default Answer;
