import React, { useState, useEffect } from "react";
import Question from "../Question/Question";
import classes from "../GameBoard/GameBoard.module.css";
import { Clues } from "../../Data/CluesInterface";

type Props = {
  value: Number | string;
  id: Number;
  changeClueValue: () => void;
  displayClue: boolean;
  pages: Number;
  categories: Number;
  markQ: any;
};

const Categories: React.FC<Props> = ({
  value,
  id,
  changeClueValue,
  displayClue,
  pages,
  markQ,
}) => {
  const [data, setData] = useState<Clues["data"]>();

  useEffect(() => {
    async function fetchData() {
      const res = await fetch(
        `https://jservice.io/api/clues?category=${id}&offset=${pages}`
      );

      if (!res.ok) {
        throw Error("Something went wrong");
      }
      const data = await res.json();
      setData(data);
      console.log("categories========>", data);
    }
    fetchData();
  }, [id, pages]);

  return (
    <>
      <div>
        <div className={classes.Board_money}>{value}</div>
        {data && (
          <div>
            {data.map((i) => {
              return (
                <Question
                  onClueClick={changeClueValue}
                  markQ2={markQ}
                  clue={displayClue}
                  key={i.id}
                  money={i.value}
                  dataClue={data}
                />
              );
            })}
          </div>
        )}
      </div>
    </>
  );
};

export default Categories;
