import React, { useState } from "react";
import GameBoard from "./components/GameBoard/GameBoard";
import CustButton from "./UI/Button/Button";

function App() {
  const [render, setRender] = useState(true);
  const [marked, setMarked] = useState(false);

  const getMarked = () => {
    setMarked(true);
  };

  const rendering = () => {
    setRender(!render);
    setMarked(false);
  };

  return (
    <div>
      <GameBoard key={render} markQ={getMarked} />
      <div className="reset_div">
        <CustButton
          className="reset_btn"
          disabled={!marked}
          onClick={() => rendering()}
        >
          RESET
        </CustButton>
      </div>
    </div>
  );
}

export default App;
